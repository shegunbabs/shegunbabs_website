const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
require('laravel-mix-purgecss');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/shegunbabs.js', 'public/js')
    .extract(['vue', 'axios'])
    //.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/tailwind.scss', 'public/css')
    .sass('resources/sass/saas.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    })
    .purgeCss()

    .browserSync({
        proxy: 'shegunbabs.web',
    });

;