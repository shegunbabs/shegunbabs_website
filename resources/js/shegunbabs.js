window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.Vue = require('vue');

Vue.component('section-header', require('./components/SectionHeader.vue').default);
Vue.component('section-sub-header', require('./components/SectionSubHeader.vue').default);
Vue.component('modal', require('./components/Modal.vue').default);
Vue.component('modal-contact-form', require('./components/ModalContactForm.vue').default);


const app = new Vue({
    el: '#app',
});
