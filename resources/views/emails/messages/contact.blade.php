@component('mail::message')
# Web ContactForm email

This message was sent through from

Name: {{ $name ?? '' }},

Email: {{ $email }}

Message: {{ $message }}

Date/Time: {{ $now }}

Ip: {{ $ip ?? '' }}

========== end ==========

Thanks,<br>
{{ config('app.name') }}
@endcomponent
