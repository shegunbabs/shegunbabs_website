<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Saas Admin Template</title>
    <link rel="stylesheet" href="/css/saas.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
</head>
<body>
<div id="app">
    <div class="wrapper font-poppins flex flex-wrap min-h-screen">

        <nav id="menu" class="w-1/5 bg-white text-sm shadow-2xl overflow-hidden">
            <div class="flex">
                <header class="flex flex-wrap h-full">
                    <div class="border-b py-4 w-full">
                        <h2 class="text-blue-500 font-medium text-base text-center">EasyCreditNg</h2>
                    </div>

                    <div class="header-body flex flex-wrap">
                        <div class="user flex justify-center items-center align-centent pt-6 pb-6 ml-5">

                                <img src="https://images.unsplash.com/photo-1544725176-7c40e5a71c5e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" alt="" srcset=""
                                    class="user-avatar object-cover object-center h-12 w-12 rounded-full">

                            <div class="self-center">
                                <h4 class="user-fullname font-bold ml-4">
                                    Sierra Ferguson
                                    <span class="font-normal text-xs">s.ferguson@gmail.com</span>
                                </h4>
                            </div>
                        </div>
                        <ul class="nav-links pl-6 text-gray-600">
                            <li class="hover:text-blue-500">
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-bar-chart w-6" aria-hidden="true"></i> <span class="">Dashboard</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-tasks w-6" aria-hidden="true"></i> <span>Tasks</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-cog w-6" aria-hidden="true"></i> <span>Email</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-user w-6" aria-hidden="true"></i> <span>Contacts</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-exchange w-6" aria-hidden="true"></i> <span>Transactions</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="py-3 block font-medium">
                                    <i class="fa fa-cog w-6" aria-hidden="true"></i> <span>Settings</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </header>
            </div>
        </nav>

        <main id="panel" class="w-4/5">
            <header class="flex flex-wrap">
                <div class="border-b py-4 w-full flex items-center px-10">
                    <h3>Panel</h3>
                </div>
                <div class="content-body container px-10" style="rgba(0, 0, 0, 0.25);">
                    content body here
                </div>
            </header>
        </main>
    </div>
</div>

</body>
</html>
