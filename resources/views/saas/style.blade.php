<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Style guide</title>
    <link rel="stylesheet" href="/css/saas.css">
</head>
<body class="bg-gray-200">

    <div class="window-element container mx-auto flex flex-wrap">

        <div class="w-5/6 mx-auto mt-5 border">

            <div class="top font-text flex justify-between border-b px-4 px-4 h-10 items-center">
                <div class="title font-semibold text-lg">Styleguide</div>
                <div class="desc text-xs">Saas UI Kit ver 1.0</div>
            </div>

            <div class="content flex flex-wrap p-5">
                <h3 class="w-full mx-3 my-7 font-medium text-xl text-gray-600">Colors</h3>
                <div class="w-1/5">
                    <div class="bg-blue-600 rounded h-12 m-5"></div>

                </div>
                <div class="w-1/5">
                    <div class="bg-yellow-600 rounded h-12 m-5"></div>
                </div>
                <div class="w-1/5">
                    <div class="bg-red-600 rounded h-12 m-5"></div>
                </div>
                <div class="w-1/5">
                    <div class="bg-green-600 rounden h-12 m-5"></div>
                </div>
                <div class="w-1/5">
                    <div class="bg-purple-600 rounded h-12 m-5"></div>
                </div>
            </div>

        </div>

    </div>

</body>
</html>
