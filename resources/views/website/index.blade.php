<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>ShegunBabs | website of Lagos based Product Developer, Full-Stack, Backend PHP & JavaScript Developer</title>
    <meta name="author" content="shegunbabs">
    <meta name="description" content="The unofficial website of Shegun Babs, a Product & Web Developer whose interest is creating web & mobile solutions">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset("css/tailwind.css") }}">
    <link rel="stylesheet" href="{{ asset("css/dripicons.css") }}">
    @if(App::environment('production'))
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154984985-1"></script>
    <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-154984985-1');</script>
    @endif


</head>

<body>
    <div id="app" class="flex flex-col font-body">
        <div class="bg-gray-100 h-screen max-h-screen">
            <div class="container mx-auto h-full flex flex-col justify-between">
                <div class="nav py-10 pl-4 md:px-48 flex justify-between">
                    <div class="avatar w-16 h-16 bg-blue-700 text-white font-semibold rounded-full flex items-center justify-center text-xl">
                        SB
                    </div>
                    <ul class="nav-menus flex items-center text-gray-900">
                        <li class="px-5"><a href="{{ route('website.index') }}" class="hover:text-gray-700">About</a></li>
                        <li class="px-5 hidden md:flex"><a href="#" class="hover:text-gray-700">Work <span class="text-gray-500 text-xs"><sup>soon</sup></span></a></li>
                        <li class="px-5 hidden md:flex"><a href="#" class="hover:text-gray-700">Notes <span class="text-gray-500 text-xs"><sup>soon</sup></span></a></li>
                    </ul>
                </div>

                <div class="after-nav md:px-48 md:py-10 text-center">
                    <h3 class="font-vibes text-6xl">I'm Shegun Babs</h3>
                    <p class="md:py-10">
                        a web & product developer
                    </p>
                </div>
                <div class="illustration flex justify-center">
                    <img src="/img/mountain-svgrepo.svg" class="h-56">
                </div>
            </div>
        </div>

        <div class="container mx-auto bg-white flex flex-col items-center pb-16 h-screen max-h-screen">
            <div class="avatar w-24 h-24 bg-blue-700 text-white font-semibold
            rounded-full flex items-center justify-center text-5xl my-10">
                SB
            </div>

            <div class="about text-center md:w-1/3 px-5">
                <h3 class="text-2xl pb-10 text-blue-700">Hiya.</h3>
                <p class="text-xl">
                    I'm a software & product developer/manager based in Lagos, Nigeria.
                    <br><br>
                    I have a passion for creating & buidling solutions for both web & mobile.
                    <br><br>
                    I have 5years+ of industry experience building solutions in different industries.
                </p>
            </div>
        </div>

        <div class="bg-gray-100 pb-16 min-h-screen">
            <div class="container mx-auto">
                <section-header>What I can do.</section-header>
                <div class="section-one flex flex-wrap px-5 md:px-48">
                    <div class="w-full md:w-1/2 flex items-center pr-5">
                        <div>
                            <section-sub-header>Design your Ideas.</section-sub-header>
                            <p class="text-lg leading-relaxed">
                                From conception to design, my aim is to come up with a solution
                                which solves the pre-conceived problem in a way that fulfil expectations and/or needs
                            </p>
                        </div>
                    </div>

                    <div class="w-full flex items-center justify-around pt-3
                                md:w-1/2 md:pt-0">
                        <div class="icon dripicons-lightbulb px-3" style="color:#398FF7;font-size:3em;"></div>
                        <div class="icon dripicons-rocket px-3" style="color:#398FF7;font-size:3em;"></div>
                        <div class="icon dripicons-message px-3" style="color:#398FF7;font-size:3em;"></div>
                    </div>
                </div>

                <div class="section-one flex flex-wrap mt-10 px-5 md:px-48">
                    <div class="w-full flex items-center justify-around order-last pt-3
                                md:pr-5 md:w-1/2 md:flex-row-reverse md:order-none md:pt-0">
                        <div class="icon dripicons-wifi px-3" style="color:#398FF7;font-size:3em;"></div>
                        <div class="icon dripicons-web px-3" style="color:#398FF7;font-size:3em;"></div>
                        <div class="icon dripicons-code px-3" style="color:#398FF7;font-size:3em;"></div>
                    </div>

                    <div class="w-full flex items-center
                                md:w-1/2">
                        <div>
                            <section-sub-header>Develop your Solution.</section-sub-header>
                            <p class="text-lg leading-relaxed">
                                As a developer, I understand the requirements of your solution (be it a website, mobile app, web app)
                                and have the required knowledge, experience & epertise to make it happen
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="pb-5 px-5">
            <div class="container mx-auto">
                <section-header>I can help.</section-header>
                <div class="text-center">
                    <section-sub-header>I'm available for freelance work.</section-sub-header>
                    <p class="text-center md:w-1/3 mx-auto pb-10">
                        If you have an idea that could do our world a benefit or a project or you wanna ask me a/some question, then get intouch
                    </p>
                    <modal-contact-form>MESSAGE ME.</modal-contact-form>
                </div>
            </div>
        </div>
        <div class="footer py-16 bg-gray-200">
        </div>
    </div>

    <script src="{{ asset("js/manifest.js") }}"></script>
    <script src="{{ asset("js/vendor.js") }}"></script>
    <script src="{{ asset("js/shegunbabs.js") }}"></script>

</body>
</html>
