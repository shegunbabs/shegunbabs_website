module.exports = {
    purge: false,
    theme: {
        fontFamily: {
            'body': ['Alata'],
            'vibes': ['Vibes'],
            'text': ['Quicksand', 'Montserrat', 'Work Sans',],
            'poppins': ['Poppins']
        },
        zIndex: {
            '9998': 9998,
        },
        extend: {}
    },
    variants: {},
    plugins: []
}
