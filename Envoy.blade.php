@servers(['local' => 'vagrant@localhost -p2223', 'web' => 'shegun@shegunbabs.com -p2442'])

# envoy run push --message='git message here'
# envoy run pull-on-server
# envoy run deploy --message="git message here" --migrate --composer

@setup
    $local_app_dir = "/var/www/shegun";
    $remote_app_dir = "/var/www/shegunbabs_website";
@endsetup

###########
# STORIES #
###########
@story('deploy')
push
pull-on-server
@endstory


#########
# TASKS #
#########

#//////////////////
# PUSH TASK START #
#//////////////////
    @task('push', ['on' => 'local'])
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Start push command ==========\n"

    #///////////////////////////////
    # NAVIGATE TO PROJECT DIRECTORY #
    #///////////////////////////////
    cd {{ $local_app_dir }}

    npm run prod

    #//////////////////////
    # ADD UNTRACKED FILES #
    #//////////////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Add untracked files =========="
    git add .

    #///////////////////////////////////////////////////
    # ADD A COMMIT MESSAGE IF PASSED IN OR USE DEFAULT #
    #///////////////////////////////////////////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Start git commit =========="
    @if ($message)
        git commit -m "{{ $message }}"
    @else
        git commit -m "regular update"
    @endif

    #/////////////////////////////////////////////
    # PUSH TO MASTER/HEAD OR BRANCH AS IT MAY BE #
    #/////////////////////////////////////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Git push to master =========="
    git push -u origin master

    #print a quote
    php artisan inspire

    #insert an extra line
    printf "\n"

    @endtask
############
# END TASK #
############

#////////////////////////////
# PULL ON SERVER TASK START #
#////////////////////////////
    @task('pull-on-server', ['on' => 'web'])
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Pull on server started ==========\n"

    #/////////////////////////////
    # NAVIGATE TO PROJECT FOLDER /
    #/////////////////////////////
    cd {{ $remote_app_dir }}

    #////////////////////////////////
    # CLEAR CACHED ROUTES & CONFIGS /
    #////////////////////////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Clear cache & routes =========="
    php artisan route:clear

    #//////////////
    # DO GIT PULL /
    #//////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Git pull start =========="
    sudo git stash
    sudo git pull

    #/////////////////////
    # DO COMPOSER UPDATE /
    #/////////////////////
    @if( $composer )
        printf "$(date '+%d-%m-%Y %H-%M-%S')========== Composer update start =========="
        sudo composer install
    @endif

    #///////////////////////////
    # CACHE ROUTES AND CONFIGS /
    #///////////////////////////
    printf "$(date '+%d-%m-%Y %H-%M-%S')========== Cache route & configs =========="
    #php artisan route:cache
    #php artisan config:cache

    #/////////////////////////
    # MIGRATE PENDING TABLES /
    #/////////////////////////
    @if ( $migrate )
        printf "$(date '+%d-%m-%Y %H-%M-%S')========== Migrate pending tables =========="
        php artisan migrate --force
    @endif

@endtask