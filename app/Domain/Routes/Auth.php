<?php


namespace Domain\Routes;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;

class Auth
{

    public static function routes()
    {
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login']);

        Route::post('logout', [LoginController::class, 'logout'])->name('logout');

        Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [RegisterController::class, 'register']);

        //password reset
        Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm']);
        Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);

        Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm']);
        Route::post('password/reset', [ResetPasswordController::class, 'reset']);
    }
}