<?php

namespace Domain\Website\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'      => 'required|string|min:2|max:50',
            'email'     => 'required|email',
            'message'   => 'required|string|max:255'
        ];
    }

}
