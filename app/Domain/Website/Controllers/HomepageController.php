<?php

namespace Domain\Website\Controllers;

use Domain\Website\Actions\SendContactEmailAction;

class HomepageController
{

    public function index()
    {
        return view('website.index');
    }


    public function render(SendContactEmailAction $sendContactEmailAction)
    {
        return $sendContactEmailAction->preview([
            'name' => 'Shegun Babatunde',
            'email' => 'shegun.babs@gmail.com',
            'message' => 'Here is a just a test message.' . "\r\n" . 'It is well'
        ]);
    }
}
