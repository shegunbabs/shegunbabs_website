<?php

namespace Domain\Website\Controllers;

use Domain\Website\Actions\SendContactEmailAction;
use Domain\Website\Mail\ContactMessage;
use Domain\Website\Requests\ContactFormRequest;

class ContactController
{

    public function store(ContactFormRequest $request, SendContactEmailAction $sendContactEmailAction)
    {
        if($request->ajax())
        {
            $formValues = $request->validated();
            $formValues['ip_address'] = $request->ips();
            $sendContactEmailAction->execute($formValues);

            return response()->json(['result' => true]);
        }
    }
}
