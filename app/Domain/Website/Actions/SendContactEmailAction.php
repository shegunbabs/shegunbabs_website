<?php


namespace Domain\Website\Actions;


use Domain\Website\Mail\ContactMessage;
use Illuminate\Support\Facades\Mail;

class SendContactEmailAction
{

    public function execute(array $contactFormValues)
    {
        //send email
        Mail::to('shegun.babs@gmail.com')
            ->send(new ContactMessage($contactFormValues));
    }

    public function preview(array $contactFormValues)
    {
        return new ContactMessage($contactFormValues);
    }
}