<?php

namespace Domain\Website\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $formValues;

    public function __construct(array $formValues)
    {
        $this->formValues = $formValues;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->formValues['email'])
            ->subject('New Contact Message from: ' .$this->formValues['email'])
            ->markdown('emails.messages.contact')
        ->with([
            'name' => $this->formValues['name'],
            'email' => trim($this->formValues['email']),
            'message' => $this->formValues['message'],
            'now' => now(),
            'ip' => implode(', ', $this->formValues['ip_address'])
        ])
            ;
    }
}
