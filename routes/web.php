<?php

use Domain\Routes\Auth;
use Domain\Website\Controllers\HomepageController;
use Domain\Website\Controllers\ContactController;


Route::get('/', [HomepageController::class, 'index'])->name('website.index');
//Route::get('test-email', [HomepageController::class, 'render']);

Route::prefix('api/v1')->group(function()
{
    Route::post('contact',      [ContactController::class, 'store']);
});

#Auth::routes();

#Route::get('/home', 'HomeController@index')->name('home');

//Route::prefix('saas')->group(function()
//{
//    Route::get('index', function(){return view('saas.index');});
//});

//Route::prefix('blog')->group(function () {
//    Route::get('/', 'BlogController@getPosts')->name('blog.index');
//    Route::middleware('Canvas\Http\Middleware\Session')->get('{slug}', 'BlogController@findPostBySlug')->name('blog.post');
//    Route::get('tag/{slug}', 'BlogController@getPostsByTag')->name('blog.tag');
//    Route::get('topic/{slug}', 'BlogController@getPostsByTopic')->name('blog.topic');
//});


